**Packaging repository for Freetype**

## Example CMake Usage:
```cmake
target_link_libraries(target freetype)
target_include_directories(target PRIVATE "$<TARGET_PROPERTY:freetype,INTERFACE_INCLUDE_DIRECTORIES>") 
```